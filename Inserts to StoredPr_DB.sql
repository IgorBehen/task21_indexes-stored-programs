-- pharmacy: 
 INSERT INTO pharmacy (name, building_number, www, work_time, saturday, sunday, street_id) VALUES ('№1', '32', 'www.first.com', '20:00:00', 0, 0, 1);
 INSERT INTO pharmacy (name, building_number, www, work_time, saturday, sunday, street_id) VALUES ('№2', '11', 'www.second.com', '19:00:00', 1, 0, 3);
 INSERT INTO pharmacy (name, building_number, www, work_time, saturday, sunday, street_id) VALUES ('№3', '2', 'www.third.com', '21:00:00', 0, 0, 2);
-- employee:
INSERT INTO employee (surname, name, midle_name, identity_number, passport, experience, birthday, post_id, pharmacy_id) VALUES ('Ivanenko', 'Ivan', 'Ivanovych', '004573834', 'rv156785', 5, 20001003, 2, 2);
INSERT INTO employee (surname, name, midle_name, identity_number, passport, experience, birthday, post_id, pharmacy_id) VALUES ('Petrenko', 'Petro', 'Petrovych', '005789403', 'nf473897', 2, 19931003, 1, 1);
INSERT INTO employee (surname, name, midle_name, identity_number, passport, experience, birthday, post_id, pharmacy_id) VALUES ('Hryhorenko', 'Hryhorij', 'Hryhorovych', '002567489', 'sn453758', 7, 19871003, 3, 3);
-- medicine:
INSERT INTO medicine (name, ministry_code, recipe, narcotic, psychotropic) VALUES ('Ibuprofen', '346523', 0, 0, 0);
INSERT INTO medicine (name, ministry_code, recipe, narcotic, psychotropic) VALUES ('Lorazepam', '097857', 0, 0, 0);
INSERT INTO medicine (name, ministry_code, recipe, narcotic, psychotropic) VALUES ('Hydromorphone', '548756', 1, 1, 1);
-- post:
INSERT INTO post (post) VALUES ('manager');
INSERT INTO post (post) VALUES ('pharmacist');
INSERT INTO post (post) VALUES ('driver');
-- street
 INSERT INTO street (street) VALUES ('Shevchenka');
INSERT INTO street (street) VALUES ('Franka');
 INSERT INTO street (street) VALUES ('Lysenka');
-- zone:
 INSERT INTO zone (name) VALUES ('heart');
 INSERT INTO zone (name) VALUES ('lungs');
 INSERT INTO zone (name) VALUES ('kidneys');
-- medicine_zone:
 INSERT INTO medicine_zone (medicine_id, zone_id) VALUES (2, 1);
 INSERT INTO medicine_zone (medicine_id, zone_id) VALUES (1, 3);
 INSERT INTO medicine_zone (medicine_id, zone_id) VALUES (3, 1);
-- pharmacy_medicine:
 INSERT INTO pharmacy_medicine (pharmacy_id, medicine_id ) VALUES (1, 3);
 INSERT INTO pharmacy_medicine (pharmacy_id, medicine_id ) VALUES (3, 3);
 INSERT INTO pharmacy_medicine (pharmacy_id, medicine_id ) VALUES (2, 1);
