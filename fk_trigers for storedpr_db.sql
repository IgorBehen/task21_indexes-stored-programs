-- pharmacy: 
-- INSERT INTO pharmacy (name, building_number, www, work_time, saturday, sunday, street_id) VALUES ('№1', '32', 'www.first.com', '20:00:00', 0, 0, 1);
-- INSERT INTO pharmacy (name, building_number, www, work_time, saturday, sunday, street_id) VALUES ('№2', '11', 'www.second.com', '19:00:00', 1, 0, 3);
-- INSERT INTO pharmacy (name, building_number, www, work_time, saturday, sunday, street_id) VALUES ('№3', '2', 'www.third.com', '21:00:00', 0, 0, 2);
-- employee:
-- INSERT INTO employee (surname, name, midle_name, identity_number, passport, experience, birthday, post_id, pharmacy_id) VALUES ('Ivanenko', 'Ivan', 'Ivanovych', '004573834', 'rv156785', 5, 20001003, 2, 2);
-- INSERT INTO employee (surname, name, midle_name, identity_number, passport, experience, birthday, post_id, pharmacy_id) VALUES ('Petrenko', 'Petro', 'Petrovych', '005789403', 'nf473897', 2, 19931003, 1, 1);
-- INSERT INTO employee (surname, name, midle_name, identity_number, passport, experience, birthday, post_id, pharmacy_id) VALUES ('Hryhorenko', 'Hryhorij', 'Hryhorovych', '002567489', 'sn453758', 7, 19871003, 3, 3);
-- medicine:
-- INSERT INTO medicine (name, ministry_code, recipe, narcotic, psychotropic) VALUES ('Ibuprofen', '346523', 0, 0, 0);
-- INSERT INTO medicine (name, ministry_code, recipe, narcotic, psychotropic) VALUES ('Lorazepam', '097857', 0, 0, 0);
-- INSERT INTO medicine (name, ministry_code, recipe, narcotic, psychotropic) VALUES ('Hydromorphone', '548756', 1, 1, 1);
-- post:
-- INSERT INTO post (post) VALUES ('manager');
-- INSERT INTO post (post) VALUES ('pharmacist');
-- INSERT INTO post (post) VALUES ('driver');
-- street
-- INSERT INTO street (street) VALUES ('Shevchenka');
-- INSERT INTO street (street) VALUES ('Franka');
-- INSERT INTO street (street) VALUES ('Lysenka');
-- zone:
-- INSERT INTO zone (name) VALUES ('heart');
-- INSERT INTO zone (name) VALUES ('lungs');
-- INSERT INTO zone (name) VALUES ('kidneys');
-- medicine_zone:
-- INSERT INTO medicine_zone (medicine_id, zone_id) VALUES (2, 1);
-- INSERT INTO medicine_zone (medicine_id, zone_id) VALUES (1, 3);
-- INSERT INTO medicine_zone (medicine_id, zone_id) VALUES (3, 1);
-- pharmacy_medicine:
-- INSERT INTO pharmacy_medicine (pharmacy_id, medicine_id ) VALUES (1, 3);
-- INSERT INTO pharmacy_medicine (pharmacy_id, medicine_id ) VALUES (3, 3);
-- INSERT INTO pharmacy_medicine (pharmacy_id, medicine_id ) VALUES (2, 1);

  SELECT * FROM pharmacy;
 -- SELECT * FROM employee;
 -- SELECT * FROM medicine;
 -- SELECT * FROM post;
 -- SELECT * FROM street;
 -- SELECT * FROM zone;
 -- SELECT * FROM medicine_zone;
SELECT * FROM pharmacy_medicine;
 
 
 -- BeforeInsertEmployee - for post & pharmacy  - OK +/-m (проблема з полем pharmacy_id - глюк Workbench-у?)
/*DELIMITER //
CREATE TRIGGER BeforeInsertEmployee
BEFORE INSERT
ON employee FOR EACH ROW
BEGIN
IF (SELECT COUNT(*) FROM post WHERE  post.id=new.post_id)=0
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'There is no such post';
END IF;
IF(SELECT COUNT(*) FROM pharmacy WHERE pharmacy.id=new.pharmacy_id)=0
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'There is no such pharmacy';
END IF;
END //
DELIMITER ;*/

-- BeforeUpdateEmployee  - for post & pharmacy - OK +/-m (проблема з полем pharmacy_id - глюк Workbench-у?)
/*DELIMITER //
CREATE TRIGGER BeforeUpdateEmployee
BEFORE UPDATE
ON employee FOR EACH ROW
BEGIN
IF (SELECT COUNT(*) FROM post WHERE  post.id=new.post_id)=0
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'There is no such post';
END IF;
IF(SELECT COUNT(*) FROM pharmacy WHERE pharmacy.id=new.pharmacy_id)=0
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'There is no such pharmacy';
END IF;
END //
DELIMITER ; */

-- BeforeUpdatePost for post ??? - відбувся Update
/*DELIMITER //
CREATE TRIGGER BeforeUpdatePost 
BEFORE UPDATE 
ON post FOR EACH ROW
BEGIN
IF ( old.id <> new.id AND (SELECT COUNT(*) FROM employee WHERE employee.post_id = old.id) <> 0 ) 
THEN SIGNAL SQLSTATE '45000' 
SET MESSAGE_TEXT = 'Can\'t update record. Foreign key updates to child table restricted!';
END IF;
END //
DELIMITER ; */

-- BeforeUpdatePost2  - OK
/*DELIMITER ;;
CREATE TRIGGER BeforeUpdatePost2
BEFORE UPDATE on post
FOR EACH ROW
BEGIN
IF (NEW.id = OLD.id)
THEN
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'An id cannot be its own Parent';
END IF;
END;;
DELIMITER ;*/

-- BeforeDeletePost for post ??? - помилка
/*DELIMITER //
CREATE TRIGGER BeforeDeletePost 
BEFORE DELETE 
ON post FOR EACH ROW 
BEGIN 
    SELECT CASE
    WHEN ((SELECT post_id FROM employee WHERE post_id = OLD.id) IS NOT NULL)
    THEN RAISE(ABORT, 'delete on table "post" violates foreign key ' ||  ' ')
    END;
END //
DELIMITER ; */


-- BeforeDeletePost3 ??? - видалило
/*DELIMITER //
CREATE TRIGGER BeforeDeletePost3
   AFTER DELETE
    On post
   FOR EACH ROW
BEGIN
   IF (EXISTS (SELECT post FROM post WHERE post = OLD.post)) 
   THEN
    INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
   END IF;
END //
DELIMITER ; */

-- BeforeDeletePost4 - !OK!
/*DELIMITER //
CREATE TRIGGER BeforeDeletePost4 
BEFORE DELETE 
ON post FOR EACH ROW
BEGIN
IF ( SELECT COUNT(*) FROM employee  WHERE employee.post_id = old.id) <> 0 THEN
    SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t delete record. Foreign key exists in child table!';
END IF;
END //
DELIMITER ;*/


-- BeforeUpdatePharmacy - ??? оновило
/*DELIMITER //
CREATE TRIGGER BeforeUpdatePharmacy
BEFORE UPDATE 
on pharmacy
FOR EACH ROW
BEGIN
IF (NEW.id = OLD.id)
THEN
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'An id cannot be its own Parent';
END IF;
END //
DELIMITER ; */

-- BeforeDeletePharmacy - OK
/*DELIMITER //
CREATE TRIGGER BeforeDeletePharmacy 
BEFORE DELETE 
ON pharmacy FOR EACH ROW
BEGIN
IF ( SELECT COUNT(*) FROM employee  WHERE employee.post_id = old.id) <> 0 THEN
    SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t delete record. Foreign key exists in child table!';
END IF;
END //
DELIMITER ; */

-- BeforeInsertPharmacy - for street - !OK!
/*DELIMITER //
CREATE TRIGGER BeforeInsertPharmacy 
BEFORE INSERT
ON pharmacy FOR EACH ROW
BEGIN
IF (SELECT COUNT(*) FROM street WHERE  street.id=new.street_id)=0
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'There is no such street';
END IF;
END //
DELIMITER ; */

-- BeforeUpdatePharmacy - for street
/*DELIMITER //
CREATE TRIGGER BeforeUpdatePharmacyForStreet
BEFORE UPDATE
ON pharmacy FOR EACH ROW
BEGIN
IF (SELECT COUNT(*) FROM street WHERE  street.id=new.street_id)=0
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'There is no such post';
END IF;
END //
DELIMITER ; */

-- BeforeDeleteStreet - OK
/*DELIMITER //
CREATE TRIGGER BeforeDeleteStreet 
BEFORE DELETE 
ON street FOR EACH ROW
BEGIN
IF ( SELECT COUNT(*) FROM street  WHERE street.post_id = old.id) <> 0 THEN
    SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t delete record. Foreign key exists in child table!';
END IF;
END //
DELIMITER ; */

-- BeforeUpdateStreet  -  !OK!
/*DELIMITER //
CREATE TRIGGER BeforeUpdateStreet
BEFORE UPDATE on street
FOR EACH ROW
BEGIN
IF (NEW.id = OLD.id)
THEN
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'An id cannot be its own Parent';
END IF;
END //
DELIMITER ; */

-- InsertPharmacy_medicineForIDs  - chack for NULL pharmacy_id and medicine_id   - !OK!
/*DELIMITER //  
CREATE TRIGGER InsertPharmacy_medicineForIDs
BEFORE INSERT ON pharmacy_medicine
FOR EACH ROW
BEGIN
IF EXISTS (SELECT * FROM pharmacy_medicine 
                         LEFT OUTER JOIN   pharmacy p on pharmacy_id= p.id 
                         LEFT OUTER JOIN medicine m on medicine_id= m.id
                         WHERE m.id is NULL or p.id is NULL)
THEN
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'pharmacy_id or medicine_id is NULL';
END IF;    
END //
DELIMITER ; */

-- UpdatePharmacy_medicineForIDs  - chack for NULL pharmacy_id and medicine_id   - !OK!
/*DELIMITER //  
CREATE TRIGGER UpdatePharmacy_medicineForIDs
BEFORE UPDATE ON pharmacy_medicine
FOR EACH ROW
BEGIN
IF EXISTS (SELECT * FROM pharmacy_medicine 
                         LEFT OUTER JOIN   pharmacy p on pharmacy_id= p.id 
                         LEFT OUTER JOIN medicine m on medicine_id= m.id
                         WHERE m.id is NULL or p.id is NULL)
THEN
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'pharmacy_id or medicine_id is NULL';
END IF;    
END //
DELIMITER ; */


-- BeforeUpdatePharmacyForMedicine
/*DELIMITER //
CREATE TRIGGER BeforeUpdatePharmacyForMedicine
BEFORE UPDATE on pharmacy
FOR EACH ROW
BEGIN
IF (NEW.id = OLD.id)
THEN
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'An id cannot be its own Parent';
END IF;
END //
DELIMITER ; */

-- BeforeDeletePharmacyForMedicine 
/*DELIMITER //
CREATE TRIGGER BeforeDeletePharmacyForMedicine 
BEFORE DELETE 
ON pharmacy FOR EACH ROW
BEGIN
IF ( SELECT COUNT(*) FROM medicine  WHERE medicine.post_id = old.id) <> 0 THEN
    SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t delete record. Foreign key exists in child table!';
END IF;
END //
DELIMITER ; */

-- --------------------------------------

-- InsertForMedicine_ZoneForIDsCheckNulls - chack for NULL zone_id and medicine_id
/*DELIMITER //  
CREATE TRIGGER InsertForMedicine_ZoneForIDsCheckNulls
BEFORE INSERT ON medicine_zone
FOR EACH ROW
BEGIN
IF EXISTS (SELECT * FROM medicine_zone 
                         LEFT OUTER JOIN zone z on zone_id= z.id 
                         LEFT OUTER JOIN medicine m on medicine_id= m.id
                         WHERE m.id is NULL or z.id is NULL)
THEN
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'pharmacy_id or medicine_id is NULL';
END IF;    
END //
DELIMITER ; */

-- UpdateForMedicine_ZoneForIDsCheckNulls - chack for NULL zone_id and medicine_id
/*DELIMITER //  
CREATE TRIGGER UpdateForMedicine_ZoneForIDsCheckNulls
BEFORE UPDATE ON medicine_zone
FOR EACH ROW
BEGIN
IF EXISTS (SELECT * FROM medicine_zone
                         LEFT OUTER JOIN zone z on zone_id= z.id 
                         LEFT OUTER JOIN medicine m on medicine_id= m.id
                         WHERE m.id is NULL or z.id is NULL)
THEN
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'pharmacy_id or medicine_id is NULL';
END IF;    
END //
DELIMITER ; */


-- BeforeUpdateZoneForMedicine
/*DELIMITER //
CREATE TRIGGER BeforeUpdateZoneForMedicine
BEFORE UPDATE on zone
FOR EACH ROW
BEGIN
IF (NEW.id = OLD.id)
THEN
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'An id cannot be its own Parent';
END IF;
END //
DELIMITER ; */

-- BeforeDeleteZoneForMedicine 
/*DELIMITER //
CREATE TRIGGER BeforeDeleteZoneForMedicine 
BEFORE DELETE 
ON zone FOR EACH ROW
BEGIN
IF ( SELECT COUNT(*) FROM zone  WHERE zone.post_id = old.id) <> 0 THEN
    SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t delete record. Foreign key exists in child table!';
END IF;
END //
DELIMITER ; */

-- DROP TRIGGER IF EXISTS  InsertForMedicine_ZoneForIDsCheckNulls;
-- UPDATE pharmacy_medicine SET pharmacy_id = 4 WHERE pharmacy_id = 4;
-- INSERT INTO pharmacy_medicine (pharmacy_id, medicine_id) VALUES (4, 4);
-- DROP TRIGGER IF EXISTS InsertUpdatePharmacy_medicineForIDs;
-- UPDATE street SET street = 'Malynova' WHERE id = 1;
-- UPDATE pharmacy SET street_id = 4 WHERE street_id = 1;
-- INSERT INTO pharmacy (name, building_number, www, work_time, saturday, sunday, street_id) VALUES ('№4', '54', 'www.fourth.com', '20:00:00', 1, 0, 4);
-- DELETE FROM pharmacy WHERE id = 2;
-- UPDATE pharmacy SET id = 4 WHERE id = 1;
-- DROP TRIGGER IF EXISTS BeforeUpdatePharmacy;
-- DELETE FROM post WHERE id = 1;
-- INSERT INTO post (id, post) VALUES (1, 'manager');
-- UPDATE post SET post = 'manager' WHERE id = 1;
-- UPDATE employee SET pharmacy_id = '3' WHERE name = 'Ivan';
-- INSERT INTO employee (surname, name, midle_name, identity_number, passport, experience, birthday, post_id, `pharmacy_id`) VALUES ('Kovalenko', 'Taras', 'Tarasovych', '004574577', 'gf347685', 3, 19911003, 2, 3);
																										
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 